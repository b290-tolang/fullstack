// console.log('TGIF')

// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.
/* 
    url - this is the address which the request is to be made and source where the response will come from (endpoint)
    options - array or properties that contains the HTTP
    options - array or properties that contains the HTTP method, body of request and headers.

*/

// Get Post Data / Retrieve / Read function

fetch('https://jsonplaceholder.typicode.com/posts/')
    .then(res => res.json())
    .then(data => showPosts(data))


//  VIES POST - display each post from JSON placeholder
const showPosts = posts => {

    let postEntries = ""

    posts.forEach(post => {
        
        postEntries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>
        `
    });

    console.log(postEntries)

    document.querySelector('#div-post-entries').innerHTML = postEntries
}

// const showPost = post => {

//     let postEntries = ""

//         postEntries += `
//         <div id="post-${post.id}">
//             <h3 id="post-title-${post.id}">${post.title}</h3>
//             <p id="post-body-${post.id}">${post.body}</p>
//             <button onclick="editPost('${post.id}')">Edit</button>
//             <button onclick="deletePost('${post.id}')">Delete</button>
//         </div>
//         `
// }

// Post Data / Create Function
document.querySelector("#form-add-post").addEventListener('submit', e => {
    // Prevents the page from reloading. Also prevents the default behavior of our event.
    e.preventDefault()

    fetch("https://jsonplaceholder.typicode.com/posts/",
        {
            method: "POST",
            body: JSON.stringify({
                title: document.querySelector("#txt-title").value,
                body: document.querySelector("#txt-body").value,
                userId: 290
            }),
            headers: {
                "Content-Type" : "application/json",
            }
        }
    )
    .then(res => res.json())
    .then(data => {
        console.log(data);
        alert('Post successfully added!')
    })
    
    document.querySelector("#txt-title").value = null
    document.querySelector("#txt-body").value = null
})

// EDIT POST DATA / Edit P

const editPost = id => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML
    
    document.querySelector('#txt-edit-id').value = id
    document.querySelector("#txt-edit-title").value = title
    document.querySelector("#txt-edit-body").value = body

    document.querySelector("#btn-submit-update").removeAttribute("disabled")

}

// Update Post Data / PUT Method
document.querySelector("#form-edit-post").addEventListener('submit', e => {

    e.preventDefault()

    let id = document.querySelector("#txt-edit-id").value

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
        {
            method: "PUT",
            body: JSON.stringify({
                id: id,
                title: document.querySelector("#txt-edit-title").value,
                body: document.querySelector("#txt-edit-body").value,
                userId: 290
            }),
            headers : {
                "Content-Type" : "application/json"
            }
        }    
    ).then(res => res.json())
    .then(data => {
        console.log(data)
        alert('Post successfully updated')
    })

    // Reset input fields once submitted
    document.querySelector("#txt-edit-title").value = null
    document.querySelector("#txt-edit-body").value = null

    // Resetting disabled attribute for button
    document.querySelector("#btn-submit-update").setAttribute("disabled", true)

})
let collection = [];

// print
// enqueue
// dequeue
// front
// size
// isEmpty

// Write the queue functions below.

function print() {
    return collection
}

function enqueue(item) {
    collection[collection.length] = item
    return collection
}

function dequeue() {
    if(collection.length === 0) {
        return collection
    }

    for(let i = 1; i < collection.length; i++) {
        collection[i-1] = collection[i]
    }

    collection.length -= 1
    return collection
}

function front() {
    return collection[0]
}

function size() {
    return collection.length
}

function isEmpty() {
    return collection.length === 0
}


module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
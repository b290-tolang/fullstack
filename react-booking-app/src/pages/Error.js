import Banner from "../components/Banner"

export default function Error({error}) {
    return (
        <Banner error={error}/>
    )
}
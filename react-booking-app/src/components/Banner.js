import { Button, Col, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Banner({error = ""}) {

    if(error !== "") {
        return (
            <>
                <h1>{error}</h1>
                <p>Go back to the <Link to={"/"}>homepage</Link> </p>
            </>
        )
    }

    return (
        <Row>
            <Col className='p-5'>

            <>
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button variant='primary'>Enroll Now!</Button>
            </>

            </Col>
        </Row>
        
    )
}

/* 
      - The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.
*/